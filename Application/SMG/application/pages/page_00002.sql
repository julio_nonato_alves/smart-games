prompt --application/pages/page_00002
begin
--   Manifest
--     PAGE: 00002
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2020.10.01'
,p_release=>'20.2.0.00.20'
,p_default_workspace_id=>9599649945896452
,p_default_application_id=>101
,p_default_id_offset=>0
,p_default_owner=>'SMART_GAMES'
);
wwv_flow_api.create_page(
 p_id=>2
,p_user_interface_id=>wwv_flow_api.id(9757604251930466)
,p_name=>'Cadastro de Produto'
,p_alias=>'CADASTRO-DE-PRODUTO'
,p_page_mode=>'MODAL'
,p_step_title=>'Cadastro'
,p_autocomplete_on_off=>'OFF'
,p_group_id=>wwv_flow_api.id(9760876940930497)
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'SMART_GAMES'
,p_last_upd_yyyymmddhh24miss=>'20210628142256'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10503879497935385)
,p_plug_name=>'Cadastro de Produto'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9672430363930330)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'TABLE'
,p_query_table=>'SMG_CAD_PRODUTOS'
,p_include_rowid_column=>true
,p_is_editable=>true
,p_edit_operations=>'i:u:d'
,p_lost_update_check_type=>'VALUES'
,p_plug_source_type=>'NATIVE_FORM'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10513594813970508)
,p_plug_name=>'Pontos de Venda'
,p_parent_plug_id=>wwv_flow_api.id(10503879497935385)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9672430363930330)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10510922510935404)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(10503879497935385)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9734986305930416)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Salvar'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P2_ROWID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10509714745935401)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(10503879497935385)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9734986305930416)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:10:&SESSION.::&DEBUG.:::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10511357055935404)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(10503879497935385)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9734986305930416)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Criar'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P2_ROWID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10510549165935403)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(10503879497935385)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9734986305930416)
,p_button_image_alt=>'Excluir'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P2_ROWID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(10511658772935404)
,p_branch_action=>'f?p=&APP_ID.:10:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10504961076935397)
,p_name=>'P2_NOME'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_prompt=>'Nome'
,p_source=>'NOME'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_cMaxlength=>100
,p_field_template=>wwv_flow_api.id(9734179725930411)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10505320510935398)
,p_name=>'P2_DESCRICAO'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_prompt=>unistr('Descri\00E7\00E3o')
,p_source=>'DESCRICAO'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>600
,p_cHeight=>4
,p_field_template=>wwv_flow_api.id(9734179725930411)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10505708082935398)
,p_name=>'P2_LINK_IMAGEM'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_prompt=>'URL Imagem'
,p_source=>'LINK_IMAGEM'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>600
,p_cHeight=>4
,p_field_template=>wwv_flow_api.id(9734179725930411)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10506108066935398)
,p_name=>'P2_PRECO'
,p_source_data_type=>'NUMBER'
,p_is_required=>true
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_prompt=>unistr('Pre\00E7o')
,p_format_mask=>'FML999G999G999G999G990D00'
,p_source=>'PRECO'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_field_template=>wwv_flow_api.id(9734179725930411)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_03=>'right'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10506540251935398)
,p_name=>'P2_PLATAFORMAS'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_prompt=>'Plataformas'
,p_source=>'PLATAFORMAS'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_cMaxlength=>100
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(9734179725930411)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10506959091935399)
,p_name=>'P2_STATUS'
,p_source_data_type=>'VARCHAR2'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_default=>'Y'
,p_prompt=>'Status'
,p_source=>'STATUS'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_YES_NO'
,p_begin_on_new_line=>'N'
,p_grid_column=>11
,p_field_template=>wwv_flow_api.id(9733822345930410)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'APPLICATION'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10513052928970503)
,p_name=>'P2_ROWID'
,p_source_data_type=>'ROWID'
,p_is_primary_key=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10503879497935385)
,p_item_source_plug_id=>wwv_flow_api.id(10503879497935385)
,p_source=>'ROWID'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10513693124970509)
,p_name=>'P2_LISTA_LOJAS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10513594813970508)
,p_use_cache_before_default=>'NO'
,p_prompt=>'&nbsp'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT loja_id',
'  FROM smg_cad_produto_x_lojas l',
'    ,  smg_cad_produtos p',
' WHERE p.produto_id = l.produto_id',
'   AND p.ROWID = :P2_ROWID'))
,p_source_type=>'QUERY_COLON'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select nome d, loja_id r',
'  from SMG_CAD_LOJAS',
' where status = ''Y'''))
,p_field_template=>wwv_flow_api.id(9733822345930410)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'3'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10512558353935407)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(10503879497935385)
,p_process_type=>'NATIVE_FORM_DML'
,p_process_name=>'Process form Cadastro de Produto'
,p_attribute_01=>'REGION_SOURCE'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>unistr('A\00E7\00E3o processada com sucesso!')
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10513749547970510)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PRC_PROCESS_LOJAS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  CURSOR c_remove_lojas (c_coll_nam   IN VARCHAR2',
'                        ,c_produto_id IN NUMBER) IS',
'    SELECT rowid rwd',
'      FROM smg_cad_produto_x_lojas',
'     WHERE produto_id  = c_produto_id',
'       AND TO_CHAR(loja_id) NOT IN (SELECT (c001)',
'                              FROM apex_collections',
'                             WHERE collection_name = c_coll_nam);',
'  --',
'  CURSOR c_insere_lojas (c_coll_nam   IN VARCHAR2',
'                        ,c_produto_id IN NUMBER) IS',
'    SELECT c001 loja_id',
'      FROM apex_collections',
'     WHERE collection_name = c_coll_nam',
'       AND c001 NOT IN (SELECT TO_CHAR(loja_id)',
'       	                  FROM smg_cad_produto_x_lojas u',
'       	                 WHERE produto_id  = c_produto_id);',
'  --',
'  l_erro       VARCHAR2(1000);',
'  l_produto_ID NUMBER;',
'BEGIN',
'  --',
'  BEGIN',
'    SELECT produto_id',
'      INTO l_produto_ID',
'      FROM smg_cad_produtos p',
'     WHERE p.ROWID = :P2_ROWID;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      RETURN;',
'  END;',
'  --',
'  apex_collection.create_or_truncate_collection(''P_LISTA_LOJAS'');',
'  apex_collection.add_members(''P_LISTA_LOJAS'', apex_util.string_to_table(:P2_LISTA_LOJAS));',
'  --',
'  FOR x IN c_remove_lojas(''P_LISTA_LOJAS'', l_produto_ID) LOOP',
'    --',
'    BEGIN',
'      DELETE smg_cad_produto_x_lojas WHERE ROWID = x.rwd;',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        l_erro := SQLERRM;',
'        apex_error.add_error(p_message          => l_erro,',
'                             p_additional_info  => l_erro,',
'                             p_display_location => apex_error.c_inline_in_notification);',
'        RETURN;',
'    END;  ',
'  END LOOP;',
'  --',
'  FOR x IN c_insere_lojas(''P_LISTA_LOJAS'', l_produto_ID) LOOP',
'    --',
'    BEGIN',
'      INSERT INTO smg_cad_produto_x_lojas',
'        (produto_id, loja_id)',
'      VALUES',
'        (l_produto_ID, x.loja_id);',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        l_erro := SQLERRM;',
'        apex_error.add_error(p_message          => l_erro,',
'                             p_additional_info  => l_erro,',
'                             p_display_location => apex_error.c_inline_in_notification);',
'        RETURN;',
'    END;  ',
'  END LOOP;',
'  --',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>unistr('A\00E7\00E3o processada com sucesso!')
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(10512100435935407)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_region_id=>wwv_flow_api.id(10503879497935385)
,p_process_type=>'NATIVE_FORM_INIT'
,p_process_name=>'Initialize form Cadastro de Produto'
);
wwv_flow_api.component_end;
end;
/
