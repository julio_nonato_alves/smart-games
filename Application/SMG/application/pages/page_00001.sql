prompt --application/pages/page_00001
begin
--   Manifest
--     PAGE: 00001
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2020.10.01'
,p_release=>'20.2.0.00.20'
,p_default_workspace_id=>9599649945896452
,p_default_application_id=>101
,p_default_id_offset=>0
,p_default_owner=>'SMART_GAMES'
);
wwv_flow_api.create_page(
 p_id=>1
,p_user_interface_id=>wwv_flow_api.id(9757604251930466)
,p_name=>'Home'
,p_alias=>'HOME'
,p_step_title=>'Smart Games'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function comprarOnline () {',
'',
'    apex.message.confirm("Deseja realizar a compra online?",',
'        function (e) {',
'            if (e == true) {',
'            ',
'                apex.message.showPageSuccess("Compra realizada com sucesso.");',
'            }',
'        }',
'    );',
'',
'    ',
'}'))
,p_inline_css=>wwv_flow_string.join(wwv_flow_t_varchar2(
'.comprar-online {',
'    background-color: green;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'Y'
,p_last_updated_by=>'SMART_GAMES'
,p_last_upd_yyyymmddhh24miss=>'20210628154215'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9768498800930533)
,p_plug_name=>'Smart Games'
,p_icon_css_classes=>'app-icon'
,p_region_template_options=>'#DEFAULT#'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(9662805937930317)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_query_num_rows=>15
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10513832976970511)
,p_plug_name=>'New'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(9647275510930296)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select PRODUTO_ID,',
'       NOME,',
'       DESCRICAO,',
'       LINK_IMAGEM,',
'       TO_CHAR(PRECO, ''FML999G999G999G999G990D00'') PRECO,',
'       PLATAFORMAS,',
'       STATUS',
'  from SMG_CAD_PRODUTOS',
' where status = ''Y'''))
,p_lazy_loading=>false
,p_plug_source_type=>'NATIVE_CARDS'
,p_plug_query_num_rows_type=>'SCROLL'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_show_total_row_count=>false
);
wwv_flow_api.create_card(
 p_id=>wwv_flow_api.id(10513945462970512)
,p_region_id=>wwv_flow_api.id(10513832976970511)
,p_layout_type=>'GRID'
,p_grid_column_count=>3
,p_title_adv_formatting=>false
,p_title_column_name=>'NOME'
,p_sub_title_adv_formatting=>false
,p_sub_title_column_name=>'DESCRICAO'
,p_body_adv_formatting=>false
,p_second_body_adv_formatting=>false
,p_second_body_column_name=>'PLATAFORMAS'
,p_badge_column_name=>'PRECO'
,p_media_adv_formatting=>false
,p_media_source_type=>'STATIC_URL'
,p_media_url=>'&LINK_IMAGEM.'
,p_media_display_position=>'FIRST'
,p_media_appearance=>'WIDESCREEN'
,p_media_sizing=>'FIT'
,p_pk1_column_name=>'PRODUTO_ID'
);
wwv_flow_api.create_card_action(
 p_id=>wwv_flow_api.id(10514012260970513)
,p_card_id=>wwv_flow_api.id(10513945462970512)
,p_action_type=>'BUTTON'
,p_position=>'PRIMARY'
,p_display_sequence=>10
,p_label=>'Ver Lojas'
,p_link_target_type=>'REDIRECT_PAGE'
,p_link_target=>'f?p=&APP_ID.:50:&SESSION.::&DEBUG.::P50_PRODUTO_ID:&PRODUTO_ID.'
,p_button_display_type=>'TEXT_WITH_ICON'
,p_icon_css_classes=>'fa-map-marker'
,p_is_hot=>true
);
wwv_flow_api.create_card_action(
 p_id=>wwv_flow_api.id(10514468592970517)
,p_card_id=>wwv_flow_api.id(10513945462970512)
,p_action_type=>'BUTTON'
,p_position=>'SECONDARY'
,p_display_sequence=>20
,p_label=>'Compra online'
,p_link_target_type=>'REDIRECT_URL'
,p_link_target=>'javascript:comprarOnline();'
,p_button_display_type=>'TEXT_WITH_ICON'
,p_icon_css_classes=>'fa-cart-plus'
,p_action_css_classes=>'comprar-online'
,p_is_hot=>true
);
wwv_flow_api.component_end;
end;
/
