prompt --application/pages/page_00050
begin
--   Manifest
--     PAGE: 00050
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2020.10.01'
,p_release=>'20.2.0.00.20'
,p_default_workspace_id=>9599649945896452
,p_default_application_id=>101
,p_default_id_offset=>0
,p_default_owner=>'SMART_GAMES'
);
wwv_flow_api.create_page(
 p_id=>50
,p_user_interface_id=>wwv_flow_api.id(9757604251930466)
,p_name=>'Lojas Mapas'
,p_alias=>'LOJAS-MAPS'
,p_page_mode=>'MODAL'
,p_step_title=>'Lojas'
,p_autocomplete_on_off=>'OFF'
,p_group_id=>wwv_flow_api.id(9760876940930497)
,p_page_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'Y'
,p_last_updated_by=>'SMART_GAMES'
,p_last_upd_yyyymmddhh24miss=>'20210628152827'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(10914316500526003)
,p_plug_name=>'Lojas Maps'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9672430363930330)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select LOGX lat, LOGY lng, nome name, l.loja_id id, nome info',
'  from smg_cad_lojas l',
'     , smg_cad_produto_x_lojas p',
' where l.loja_id = p.loja_id',
'   and produto_id = :P50_PRODUTO_ID'))
,p_plug_source_type=>'PLUGIN_COM.JK64.REPORT_GOOGLE_MAP_R1'
,p_ajax_items_to_submit=>'P50_PRODUTO_ID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'400'
,p_attribute_02=>'PINS'
,p_attribute_03=>'13'
,p_attribute_04=>'PAN_ON_CLICK:PAN_ALLOWED:ZOOM_ALLOWED:SPINNER'
,p_attribute_05=>'13'
,p_attribute_22=>'ROADMAP'
,p_attribute_24=>'Y'
,p_attribute_25=>'auto'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(10514360504970516)
,p_name=>'P50_PRODUTO_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(10914316500526003)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.component_end;
end;
/
