prompt --application/pages/page_00030
begin
--   Manifest
--     PAGE: 00030
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2020.10.01'
,p_release=>'20.2.0.00.20'
,p_default_workspace_id=>9599649945896452
,p_default_application_id=>101
,p_default_id_offset=>0
,p_default_owner=>'SMART_GAMES'
);
wwv_flow_api.create_page(
 p_id=>30
,p_user_interface_id=>wwv_flow_api.id(9757604251930466)
,p_name=>'Consulta de Lojas'
,p_alias=>'CONSULTA-DE-LOJAS'
,p_step_title=>'Consulta de Lojas'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'SMART_GAMES'
,p_last_upd_yyyymmddhh24miss=>'20210628152409'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(20372072535240638)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#:t-BreadcrumbRegion--useBreadcrumbTitle'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(9681864023930337)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(9617463773930246)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(9736317061930419)
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(20372624681240641)
,p_plug_name=>'Consulta de Lojas'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(9670579116930326)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select LOJA_ID,',
'       NOME,',
'       LOGX,',
'       LOGY,',
'       DECODE(STATUS,  ''Y'', ''Ativo'', ''Inativo'') STATUS,',
'       ROWID',
'  from SMG_CAD_LOJAS'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_page_header=>'Consulta de Produtos'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(20372734062240641)
,p_name=>'Consulta de Produtos'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'C'
,p_download_formats=>'CSV:HTML:EMAIL:XLSX:PDF:RTF'
,p_detail_link=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.::P40_ROWID:#ROWID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="">'
,p_owner=>'SMART_GAMES'
,p_internal_uid=>20372734062240641
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10513268025970505)
,p_db_column_name=>'LOJA_ID'
,p_display_order=>10
,p_column_identifier=>'I'
,p_column_label=>'Loja ID'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10601802202212750)
,p_db_column_name=>'NOME'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Nome'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10513314519970506)
,p_db_column_name=>'LOGX'
,p_display_order=>30
,p_column_identifier=>'J'
,p_column_label=>'Latitude'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10513464213970507)
,p_db_column_name=>'LOGY'
,p_display_order=>40
,p_column_identifier=>'K'
,p_column_label=>'Longitude'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10603849069212752)
,p_db_column_name=>'STATUS'
,p_display_order=>50
,p_column_identifier=>'G'
,p_column_label=>'Status'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(10514171776970514)
,p_db_column_name=>'ROWID'
,p_display_order=>60
,p_column_identifier=>'L'
,p_column_label=>'Rowid'
,p_column_type=>'OTHER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(20376337839241382)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'106046'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'LOJA_ID:NOME:LOGX:LOGY:STATUS::ROWID'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(10600716196212734)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(20372072535240638)
,p_button_name=>'Criar'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9734986305930416)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Criar'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
);
wwv_flow_api.component_end;
end;
/
